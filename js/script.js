let main = document.querySelector("#contenido");
fetch("rubros.csv")
    .then(function (res) {
        return (res.text());
    })
    .then(function (data) {
        cargaRubros(data);
    })

fetch("listas.csv")
    .then(function (res) {
        return (res.text());
    })
    .then(function (data) {
        mostrarData(data);
    });



const rubros = [];

function cargaRubros(data_rubros) {
    let filas_rubros = data_rubros.split(/\r?\n|\r/), datos_rubros;
    // console.log(filas_rubros);
    for (let i = 0; i < filas_rubros.length; i++) {
        datos_rubros = filas_rubros[i].split(",");
        // console.log(datos_rubros);
        rubros[i] = { codigo: datos_rubros[0], descripcion: datos_rubros[1], fondo: datos_rubros[2], color: datos_rubros[3] }
        // console.log(rubros[i].codigo, rubros[i].descripcion, rubros[i].fondo, rubros[i].color);
    }
    // console.log(rubros);
}

// console.log(rubros);
// console.log(rubros[0].codigo," - ",rubros[0].descripcion);
// console.log(rubros.length);
function mostrarData(contenido) {
    // console.log(rubros[0].codigo);
    // console.log(rubros.length);
    let template = ``;
    // console.log(rubros.length);
    for (let r = 0; r < rubros.length; r++) {
        let descripcion = removeAccents(rubros[r].descripcion).toLowerCase();
        let descripMayus = rubros[r].descripcion.toUpperCase();

        template += `<section class="parallax" id="${descripcion}">`;
        template += `<h3>${descripMayus}</h3>`;
        template += `<div class="contenedor">`;

        template += armarTemplateArticulos(r, contenido);

        template += `</div>`;
        template += `</section>`;
    }

    main.innerHTML = template;

    for (let r = 0; r < rubros.length; r++) {
        let descripcion = removeAccents(rubros[r].descripcion).toLowerCase();
        let seccion = document.querySelector(`#${descripcion}`);
        console.log(seccion);
        seccion.style.backgroundImage = `linear-gradient(to right, rgba(255,255,255, 0.3), rgba(255,255,255, 0.3)), url(${rubros[r].fondo})`;
        seccion.style.color = `${rubros[r].color}`;
    }


}

function armarTemplateArticulos(r, contenido) {
    let filas = contenido.split(/\r?\n|\r/);
    // console.log("Rubro: ",r," Filas: ", filas);
    let subTemplate = ``;
    for (let i = 0; i < filas.length; i++) {
        let celdasFila = filas[i].split(',');
        if (celdasFila[0] === (r + 1).toString()) {
            // console.log("Artículo: ", celdasFila[2]);
            subTemplate += `<article>`;
            subTemplate += `<img src="${celdasFila[1]}" alt="" />`
            subTemplate += `<h4>${celdasFila[2]}</h4>`
            subTemplate += `</article>`;
        }
    }
    return subTemplate;
}

//fuente: https://ricardometring.com/javascript-replace-special-characters
const removeAccents = (str) => {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
};




//fuente de seccion.style.backgroundImage: http://w3.unpocodetodo.info/css3/css-en-javascript.php#:~:text=En%20JavaScript%20podemos%20utilizar%20la,querySelector(%22ul%22)%3B

